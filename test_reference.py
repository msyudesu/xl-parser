#   Functions from previous version, for reference or re-use
self.populateOperators()

self.btn_fileSelect.clicked.connect(self.file_Selection)
self.btn_loadFileData.clicked.connect(self.populateOperatorData)
self.btn_getTotalRecords.clicked.connect(self.getTotalOperatorRecordData)
self.btn_getRecordsByPrefix.clicked.connect(self.getRecordsByPrefix)


def file_Selection(self):
    filepath, _ = QFileDialog.getOpenFileName() #   _ via tuple unpacking to elimate the file filters from WinExplorer
    url = QUrl.fromLocalFile(filepath)
    self.lbl_filename.setText(url.fileName())
    self.lbl_filename.adjustSize()
    self.populateOperatorData()
def populateOperators(self):
    operator_df = pd.read_excel("Data\proper_keys.xlsx")
    for index, row in operator_df.iterrows():
        self.cb_operators.addItem(row['KEY'])
    
def populateOperatorData(self):
    self.df = pd.read_excel(f"Data\{self.lbl_filename.text()}")
    prefixes = self.df['Prefix'].unique()
    for i in range(0, len(prefixes)):
        if isinstance(prefixes[i], str):
            self.cb_prefixes.addItem(prefixes[i])
def getTotalOperatorRecordData(self):
    self.query = self.df.loc[self.df['QC Oper'] == self.cb_operators.currentText()]
    self.calcRecordData()
    self.setOperatorDataFields()
def getRecordsByPrefix(self):
    self.query = self.df.loc[(self.df['QC Oper'] == self.cb_operators.currentText()) & (self.df['Prefix'] == self.cb_prefixes.currentText())]
    self.calcRecordData()
    self.setOperatorDataFields()
    self.getRecordsByDate()
def calcRecordData(self):
    self.total_records = len(self.query)
    self.total_pass = self.query['Pass Test'].sum()
    self.total_injections = self.query['Tot Tests'].sum()
    self.operator_yield = (self.total_pass / self.total_records).round(4)
def setOperatorDataFields(self):
    self.lbl_totalRecords.setText(f"Number of Records: {self.total_records}")
    self.lbl_totalRecords.adjustSize()
    self.lbl_passedRecords.setText(f"Passed Tests: {self.total_pass}")
    self.lbl_passedRecords.adjustSize()
    self.lbl_yield.setText(f"Yield: {self.operator_yield*100}%")
    self.lbl_yield.adjustSize()
    self.lbl_totalinjections.setText(f"Total Injections: {self.total_injections}")
    self.lbl_totalinjections.adjustSize()
    
def getRecordsByDate(self):
    self.translateDate()
    self.query = self.df.loc[self.df['Date'] == self.startDate]
    self.setOperatorDataFields()
    
def translateDate(self):
    self.qtDate = self.dateSelect.date()
    self.startDate = self.qtDate.toPyDate()
    print(self.startDate)