from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QFileDialog, QDialog, QMessageBox
from PyQt5.QtCore import QUrl
from bl_main import QCParser
import pandas as pd
import openpyxl
import sys
from enum import Enum

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1109, 484)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.lbl_window_title = QtWidgets.QLabel(self.centralwidget)
        self.lbl_window_title.setGeometry(QtCore.QRect(10, 10, 171, 31))
        font = QtGui.QFont()
        font.setFamily("Cambria")
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        self.lbl_window_title.setFont(font)
        self.lbl_window_title.setObjectName("lbl_window_title")
        self.btn_fileSelectOpen = QtWidgets.QPushButton(self.centralwidget)
        self.btn_fileSelectOpen.setGeometry(QtCore.QRect(10, 50, 131, 31))
        self.btn_fileSelectOpen.setObjectName("btn_fileSelectOpen")
        self.cb_operators = QtWidgets.QComboBox(self.centralwidget)
        self.cb_operators.setGeometry(QtCore.QRect(10, 100, 141, 31))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.cb_operators.setFont(font)
        self.cb_operators.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.cb_operators.setAutoFillBackground(False)
        self.cb_operators.setObjectName("cb_operators")
        self.dateSelect_start = QtWidgets.QDateEdit(self.centralwidget)
        self.dateSelect_start.setGeometry(QtCore.QRect(160, 100, 131, 31))
        self.dateSelect_start.setDateTime(QtCore.QDateTime.currentDateTime())
        font = QtGui.QFont()
        font.setPointSize(12)
        self.dateSelect_start.setFont(font)
        self.dateSelect_start.setAlignment(QtCore.Qt.AlignCenter)
        self.dateSelect_start.setCalendarPopup(True)
        self.dateSelect_start.setObjectName("dateSelect_start")
        self.tabWidget = QtWidgets.QTabWidget(self.centralwidget)
        self.tabWidget.setGeometry(QtCore.QRect(550, 10, 551, 421))
        self.tabWidget.setObjectName("tabWidget")
        self.tab = QtWidgets.QWidget()
        self.tab.setObjectName("tab")
        self.tab1_chart = QtWidgets.QLabel(self.tab)
        self.tab1_chart.setGeometry(QtCore.QRect(10, 10, 521, 381))
        self.tab1_chart.setAlignment(QtCore.Qt.AlignCenter)
        self.tab1_chart.setObjectName("tab1_chart")
        self.tabWidget.addTab(self.tab, "")
        self.tab_2 = QtWidgets.QWidget()
        self.tab_2.setObjectName("tab_2")
        self.table_DataOutput = QtWidgets.QTableWidget(self.tab_2)
        self.table_DataOutput.setGeometry(QtCore.QRect(0, 0, 541, 391))
        self.table_DataOutput.setObjectName("table_DataOutput")
        self.table_DataOutput.setColumnCount(0)
        self.table_DataOutput.setRowCount(0)
        self.tabWidget.addTab(self.tab_2, "")
        self.group_operatorDetails = QtWidgets.QGroupBox(self.centralwidget)
        self.group_operatorDetails.setGeometry(QtCore.QRect(10, 180, 531, 251))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.group_operatorDetails.setFont(font)
        self.group_operatorDetails.setObjectName("group_operatorDetails")
        self.lbl_totalRecords = QtWidgets.QLabel(self.group_operatorDetails)
        self.lbl_totalRecords.setGeometry(QtCore.QRect(10, 30, 181, 31))
        self.lbl_totalRecords.setObjectName("lbl_totalRecords")
        self.lbl_yield = QtWidgets.QLabel(self.group_operatorDetails)
        self.lbl_yield.setGeometry(QtCore.QRect(10, 210, 161, 31))
        self.lbl_yield.setObjectName("lbl_yield")
        self.lbl_totalinjections = QtWidgets.QLabel(self.group_operatorDetails)
        self.lbl_totalinjections.setGeometry(QtCore.QRect(10, 180, 171, 31))
        self.lbl_totalinjections.setObjectName("lbl_totalinjections")
        self.lbl_totalPassed = QtWidgets.QLabel(self.group_operatorDetails)
        self.lbl_totalPassed.setGeometry(QtCore.QRect(10, 150, 171, 31))
        self.lbl_totalPassed.setObjectName("lbl_totalPassed")
        self.lbl_highestPrefixYield = QtWidgets.QLabel(self.group_operatorDetails)
        self.lbl_highestPrefixYield.setGeometry(QtCore.QRect(200, 150, 161, 31))
        self.lbl_highestPrefixYield.setObjectName("lbl_highestPrefixYield")
        self.lbl_bestPrefix = QtWidgets.QLabel(self.group_operatorDetails)
        self.lbl_bestPrefix.setGeometry(QtCore.QRect(200, 180, 161, 31))
        self.lbl_bestPrefix.setObjectName("lbl_bestPrefix")
        self.lbl_mainColumnType = QtWidgets.QLabel(self.group_operatorDetails)
        self.lbl_mainColumnType.setGeometry(QtCore.QRect(10, 60, 381, 31))
        self.lbl_mainColumnType.setObjectName("lbl_mainColumnType")
        self.dateSelect_end = QtWidgets.QDateEdit(self.centralwidget)
        self.dateSelect_end.setGeometry(QtCore.QRect(160, 140, 131, 31))
        self.dateSelect_end.setDateTime(QtCore.QDateTime.currentDateTime())
        font = QtGui.QFont()
        font.setPointSize(12)
        self.dateSelect_end.setFont(font)
        self.dateSelect_end.setAlignment(QtCore.Qt.AlignCenter)
        self.dateSelect_end.setCalendarPopup(True)
        self.dateSelect_end.setObjectName("dateSelect_end")
        self.cb_prefixes = QtWidgets.QComboBox(self.centralwidget)
        self.cb_prefixes.setGeometry(QtCore.QRect(10, 140, 141, 31))
        self.cb_prefixes.setObjectName("cb_prefixes")
        self.btn_refresh = QtWidgets.QPushButton(self.centralwidget)
        self.btn_refresh.setGeometry(QtCore.QRect(410, 150, 131, 31))
        self.btn_refresh.setObjectName("btn_refresh")
        self.group_Options = QtWidgets.QGroupBox(self.centralwidget)
        self.group_Options.setGeometry(QtCore.QRect(300, 20, 241, 111))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.group_Options.setFont(font)
        self.group_Options.setObjectName("group_Options")
        self.checkbox_filterDate = QtWidgets.QCheckBox(self.group_Options)
        self.checkbox_filterDate.setGeometry(QtCore.QRect(10, 20, 131, 17))
        self.checkbox_filterDate.setObjectName("checkbox_filterDate")
        self.checkbox_filterDateRange = QtWidgets.QCheckBox(self.group_Options)
        self.checkbox_filterDateRange.setGeometry(QtCore.QRect(10, 40, 131, 17))
        self.checkbox_filterDateRange.setObjectName("checkbox_filterDateRange")
        self.checkbox_filterPrefix = QtWidgets.QCheckBox(self.group_Options)
        self.checkbox_filterPrefix.setGeometry(QtCore.QRect(10, 60, 131, 17))
        self.checkbox_filterPrefix.setObjectName("checkbox_filterPrefix")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1109, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        self.cb_operators.setCurrentIndex(-1)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        #   Above is Auto Generated
        self._populate_operators()
        
        self.btn_fileSelectOpen.clicked.connect(self.file_Selection)
        self.btn_refresh.clicked.connect(self.refresh_snapshot)
    
    def _display_error_window(self, title, message):
        warning_dialog = QMessageBox(MainWindow)
        warning_dialog.setWindowTitle(title)
        warning_dialog.setText(message)
        warning_dialog.setIcon(QMessageBox.Critical)
        warning_dialog.exec_()

    def _populate_operators(self):
        #   This occurs once at startup. Do not call again.
        try:
            operator_df = pd.read_excel("Data\proper_keys.xlsx")
            for index, row in operator_df.iterrows():
                self.cb_operators.addItem(row['KEY'])
        except FileNotFoundError as e:
            self._display_error_window("File Not Found", "Unable to locate 'proper_keys.xlsx'. This excel file is required to be inside inside the 'Data' folder.")
            sys.exit()

    def populate_prefixs(self, prefixes):
        self.cb_prefixes.clear()        
        prefixes = sorted([ prefix for prefix in prefixes if str(prefix) != 'nan'])
        for i in range(0, len(prefixes)):
            if isinstance(prefixes[i], str):
                self.cb_prefixes.addItem(prefixes[i])

    def file_Selection(self):
        try: 
            filepath, _ = QFileDialog.getOpenFileName()
            url = QUrl.fromLocalFile(filepath)
            self.qcparser = QCParser(url.fileName())
        except FileNotFoundError as e:
            self._display_error_window("File Not Found", "The filename you entered doesn't exist in this directory, or you did not enter a filename.")

    def checkbox_check(self):
        ck_date_status = self.checkbox_filterDate.isChecked()
        ck_dateR_status = self.checkbox_filterDateRange.isChecked()
        ck_prefix_status = self.checkbox_filterPrefix.isChecked()
        return (ck_date_status, ck_dateR_status, ck_prefix_status)

    def refresh_snapshot(self):
        try:
            operator = str(self.cb_operators.currentText())
            prefix = str(self.cb_prefixes.currentText())
            settings = self.checkbox_check()
            s_date = self.dateSelect_start.date().toPyDate()  #   Will be Current date by default.
            e_date = self.dateSelect_start.date().toPyDate()
            filtered_df = self.qcparser.get_filtered_dataframe(operator=operator, start_date=s_date, end_date=e_date, prefix=prefix, settings=settings)
            self.populate_prefixs(filtered_df['Prefix'].unique())

            metrics = self.qcparser.get_metrics(operator)

            class Metrics(Enum):
                TOTAL_RECORDS = 0
                TOTAL_PASSED = 1
                TOTAL_INJECTIONS = 2
                TOTAL_YIELD = 3
                BEST_PREFIX = 4
                PRIMARY_COLUMN_TYPE = 5      

            self.lbl_totalRecords.setText(f"Number of Records: {metrics[Metrics.TOTAL_RECORDS.value]}")
            self.lbl_mainColumnType.setText(f"Main Column Type Produced: [{metrics[Metrics.PRIMARY_COLUMN_TYPE.value]}]")
            self.lbl_totalPassed.setText(f"Total Passed: {metrics[Metrics.TOTAL_PASSED.value]}")
            self.lbl_totalinjections.setText(f"Total Injections: {metrics[Metrics.TOTAL_INJECTIONS.value]}")
            self.lbl_yield.setText(f"Yield: {metrics[Metrics.TOTAL_YIELD.value]*100}%")
            self.lbl_highestPrefixYield.setText(f"Highest Prefix Yield: {metrics[Metrics.BEST_PREFIX.value][1]*100}%")
            self.lbl_highestPrefixYield.adjustSize()
            self.lbl_bestPrefix.setText(f"Best Prefix: {metrics[Metrics.BEST_PREFIX.value][0]}")
        except AttributeError as e:
            self._display_error_window("No File Loaded", "There is currently no file loaded. Please load a QC file and try again.")
        except UnboundLocalError as e:
            self._display_error_window("No File Loaded", "There is currently no file loaded. Please load a QC file and try again.")

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "QC Log Analysis Tool"))
        self.lbl_window_title.setText(_translate("MainWindow", "QC Log Analysis"))
        self.btn_fileSelectOpen.setText(_translate("MainWindow", "Open File"))
        self.tab1_chart.setText(_translate("MainWindow", "chart img here"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), _translate("MainWindow", "Chart"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), _translate("MainWindow", "Data"))
        self.group_operatorDetails.setTitle(_translate("MainWindow", "Operator Snapshot"))
        self.lbl_totalRecords.setText(_translate("MainWindow", "Number of Records:"))
        self.lbl_yield.setText(_translate("MainWindow", "Yield:"))
        self.lbl_totalinjections.setText(_translate("MainWindow", "Total Injections:"))
        self.lbl_totalPassed.setText(_translate("MainWindow", "Total Passed:"))
        self.lbl_highestPrefixYield.setText(_translate("MainWindow", "Highest Prefix Yield:"))
        self.lbl_bestPrefix.setText(_translate("MainWindow", "Best Prefix:"))
        self.lbl_mainColumnType.setText(_translate("MainWindow", "Main Column Type Produced:"))
        self.btn_refresh.setText(_translate("MainWindow", "Refresh"))
        self.group_Options.setTitle(_translate("MainWindow", "Options"))
        self.checkbox_filterDate.setText(_translate("MainWindow", "Filter Date?"))
        self.checkbox_filterDateRange.setText(_translate("MainWindow", "Filter Date Range?"))
        self.checkbox_filterPrefix.setText(_translate("MainWindow", "Filter Prefix?"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    #styles
    stylesheetFile = "main.css"
    stylesheet = open(stylesheetFile, 'r')
    styles = stylesheet.read()
    # 
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    ##
    app.setStyleSheet(styles)
    #
    sys.exit(app.exec_())
