#   https://realpython.com/working-with-large-excel-files-in-pandas/
#   Read for reference. This project will require pandas as the records analyzed will exceed 1 million rows.

import pandas as pd
import numpy, openpyxl, sys, os

class QCParser(object):
    def __init__(self, filename):
        self.filename = filename
        self.generate_df()

    def generate_df(self):
        self.df = pd.read_excel(f'Data\{self.filename}')

    def get_filtered_dataframe(self, operator, start_date, end_date, prefix, settings):
        self.filtered_df = self.df.loc[self.df['QC Oper'] == operator]

        #   Settings indicies:
        #   0 date filter checkbox
        #   1 date range filter checkbox
        #   2 prefix filter checkbox
        if settings[0] == True and settings[1] == True:
            self.filtered_df = self.filtered_df.set_index(['Date'])
            self.filtered_df = self.filtered_df.loc[str(start_date):str(end_date)]
        elif settings[0] == True:
            self.filtered_df = self.filtered_df.loc[self.filtered_df['Date'] == str(start_date)]
        if settings[2] == True:
            self.filtered_df = self.filtered_df.loc[self.filtered_df['Prefix'] == prefix]
        
        return self.filtered_df

    def get_metrics(self, operator):
        total_records = len(self.filtered_df)
        total_passed = int(self.filtered_df['Status'].str.contains('PASS').sum())
        total_yield = round((total_passed / total_records), 4) if total_records > 0 else 0
        total_injections = self.filtered_df['Tot Tests'].sum()
        best_prefix = self._highest_prefix_with_yield(operator)
        primary_column_type = self.filtered_df['Column Type'].mode()[0] if len(self.filtered_df) > 0 else "None"
        return (total_records, total_passed, total_injections, total_yield, best_prefix, primary_column_type)

    def _highest_prefix_with_yield(self, operator):
        _prefixes = self.filtered_df['Prefix'].unique()
        results = []

        #   TODO:
        #   Manually calculate yield per prefix.
        #   Yield is not calculated properly in source doc.

        try:
            _top_yield = 0.0
        except ZeroDivisionError:
            print("Number of Records was 0. Unable to calculated top yield")
        else:
            _top_yield = 0.0
        return (0,0)

    def generate_chart(self):
        pass